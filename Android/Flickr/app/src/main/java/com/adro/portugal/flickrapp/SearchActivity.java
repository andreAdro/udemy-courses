package com.adro.portugal.flickrapp;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.widget.SearchView;
import android.widget.SearchView.OnCloseListener;
import android.widget.SearchView.OnQueryTextListener;

public class SearchActivity extends BaseActivity {

  private static final String TAG = "SearchActivity";
  private SearchView searchView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_search);

    activateToolbar(true);
    Log.d(TAG, "onCreate: ends");
//    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//    setSupportActionBar(toolbar);
//
//    FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//    fab.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View view) {
//        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//            .setAction("Action", null)
//            .show();
//      }
//    });
//    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    Log.d(TAG, "onCreateOptionsMenu: starts");
    getMenuInflater().inflate(R.menu.menu_search, menu);

    SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
    searchView = (SearchView) menu.findItem(R.id.app_bar_search)
        .getActionView();
    SearchableInfo searchableInfo = searchManager.getSearchableInfo(getComponentName());
    searchView.setSearchableInfo(searchableInfo);
//    Log.d(TAG, "onCreateOptionsMenu: " + getComponentName().toString());
//    Log.d(TAG, "onCreateOptionsMenu: hint is " + searchView.getQueryHint());
//    Log.d(TAG, "onCreateOptionsMenu: searchable info is " + searchableInfo.toString());
    searchView.setIconified(false);

    searchView.setOnQueryTextListener(new OnQueryTextListener() {
      @Override
      public boolean onQueryTextSubmit(String query) {
        Log.d(TAG, "onQueryTextSubmit: called");
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(
            getApplicationContext());
        sharedPreferences.edit()
            .putString(FLICKR_QUERY, query)
            .apply();
        finish();
        return true;
      }

      @Override
      public boolean onQueryTextChange(String newText) {
        return false;
      }
    });

    searchView.setOnCloseListener(new OnCloseListener() {
      @Override
      public boolean onClose() {
        finish();
        return false;
      }
    });

    Log.d(TAG, "onCreateOptionsMenu: returned");
    return true;
  }
}

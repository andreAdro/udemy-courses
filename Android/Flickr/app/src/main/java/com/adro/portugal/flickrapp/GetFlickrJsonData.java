package com.adro.portugal.flickrapp;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import com.adro.portugal.flickrapp.GetRawData.OnDownloadComplete;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class GetFlickrJsonData extends AsyncTask<String, Void, List<Photo>> implements OnDownloadComplete {

  private static final String TAG = "GetFlickrJsonData";
  private List<Photo> photoList;
  private String baseUrl;
  private String language;
  private boolean matchAll;

  private final OnDataAvailable callBack;
  private boolean runningOnSameThread;

  interface OnDataAvailable {

    void onDataAvailable(List<Photo> data, DownloadStatus status);
  }

  public GetFlickrJsonData(OnDataAvailable callBack, String baseUrl, String language,
      boolean matchAll) {
    Log.d(TAG, "GetFlickrJsonData called");
    this.baseUrl = baseUrl;
    this.language = language;
    this.matchAll = matchAll;
    this.callBack = callBack;
  }

  void executeOnSameThread(String searchCriteria) {
    Log.d(TAG, "executeOnSameThread starts");
    runningOnSameThread = true;
    String destinationUri = createUri(searchCriteria, language, matchAll);

    GetRawData getRawData = new GetRawData(this);
    getRawData.execute(destinationUri);
    Log.d(TAG, "executeOnSameThread ends");
  }

  @Override
  protected void onPostExecute(List<Photo> photos) {
    Log.d(TAG, "onPostExecute starts");
    if (callBack != null) {
      callBack.onDataAvailable(photoList, DownloadStatus.OK);
    }
    Log.d(TAG, "onPostExecute ends");
  }

  @Override
  protected List<Photo> doInBackground(String... params) {
    Log.d(TAG, "doInBackground starts");
    String destinationUri = createUri(params[0], language, matchAll);

    GetRawData getRawData = new GetRawData(this);
//    getRawData.execute(destinationUri); //TODO test
    getRawData.runInSameThread(destinationUri);
    Log.d(TAG, "doInBackground ends");
    return photoList;
  }

  private String createUri(String searchCriteria, String language, boolean matchAll) {
    Log.d(TAG, "createUri starts");

    return Uri.parse(baseUrl)
        .buildUpon()
        .appendQueryParameter("tags", searchCriteria)
        .appendQueryParameter("tagmode", matchAll ? "ALL" : "ANY")
        .appendQueryParameter("lang", language)
        .appendQueryParameter("format", "json")
        .appendQueryParameter("nojsoncallback", "1")
        .build()
        .toString();
  }

  @Override
  public void onDownloadComplete(String data, DownloadStatus status) {
    Log.d(TAG, "onDownloadComplete stats. Status" + status);

    if (status == DownloadStatus.OK) {
      photoList = new ArrayList<>();
      try {
        JSONObject jsonData = new JSONObject(data);
        JSONArray itemArray = jsonData.getJSONArray("items");
        for (int i = 0; i < itemArray.length(); i++) {
          JSONObject jsonPhoto = itemArray.getJSONObject(i);
          String title = jsonPhoto.getString("title");
          String author = jsonPhoto.getString("author");
          String authorId = jsonPhoto.getString("author_id");
          String tags = jsonPhoto.getString("tags");
          JSONObject jsonMedia = jsonPhoto.getJSONObject("media");
          String photoUrl = jsonMedia.getString("m");

          String link = photoUrl.replaceFirst("_m.", "_b.");

          Photo photo = Photo.builder()
              .author(author)
              .authorId(authorId)
              .image(photoUrl)
              .link(link)
              .tags(tags)
              .title(title)
              .build();
          photoList.add(photo);
          Log.d(TAG, "onDownloadComplete " + photo.toString());
        }
      } catch (JSONException e) {
        Log.e(TAG, "onDownloadComplete: Error processing Json data " + e.getMessage());
        status = DownloadStatus.FAILED_OR_EMPTY;
      }
    }
    if (runningOnSameThread && callBack != null) {
      // now inform the caller that processing is done - possibly returning null if there was an error
      callBack.onDataAvailable(photoList, status);
    }
    Log.d(TAG, "onDownloadComplete ends");
  }


}

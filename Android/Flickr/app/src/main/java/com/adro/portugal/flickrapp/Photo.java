package com.adro.portugal.flickrapp;

import java.io.Serializable;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
class Photo implements Serializable {

  private static final long serialVersionUID = 1L;
  private String author;
  private String authorId;
  private String image;
  private String link;
  private String tags;
  private String title;
}

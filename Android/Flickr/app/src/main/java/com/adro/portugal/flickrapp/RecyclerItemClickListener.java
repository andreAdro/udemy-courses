package com.adro.portugal.flickrapp;

import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;

class RecyclerItemClickListener extends RecyclerView.SimpleOnItemTouchListener {

  private static final String TAG = "RecyclerItemClickListen";
  private final OnRecyclerClickListener listener;
  private final GestureDetectorCompat gestureDetector;

  interface OnRecyclerClickListener {

    void onItemClick(View view, int position);

    void onItemLongClick(View view, int position);
  }

  public RecyclerItemClickListener(Context context, final RecyclerView recyclerView,
      final OnRecyclerClickListener listener) {
    this.listener = listener;
    this.gestureDetector = new GestureDetectorCompat(context, new SimpleOnGestureListener() {
      @Override
      public boolean onSingleTapUp(MotionEvent e) {
        Log.d(TAG, "onSingleTapUp: starts");
//        return super.onSingleTapUp(e);
        View childView = recyclerView.findChildViewUnder(e.getX(), e.getY());
        if (childView != null && listener != null) {
          Log.d(TAG, "onSingleTapUp: calling listener.onItemClick");
          listener.onItemClick(childView, recyclerView.getChildAdapterPosition(childView));
        }
        return true;
      }

      @Override
      public void onLongPress(MotionEvent e) {
        Log.d(TAG, "onLongPress: starts");
//        super.onLongPress(e);
        View childView = recyclerView.findChildViewUnder(e.getX(), e.getY());
        if (childView != null && listener != null) {
          Log.d(TAG, "onLongPress: calling listener.onItemLongClick");
          listener.onItemLongClick(childView, recyclerView.getChildAdapterPosition(childView));
        }
      }
    });
  }

  @Override
  public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
    Log.d(TAG, "onInterceptTouchEvent: starts");
//    return super.onInterceptTouchEvent(rv, e);
    if (gestureDetector != null) {
      boolean result = gestureDetector.onTouchEvent(e);
      Log.d(TAG, "onInterceptTouchEvent: returned: " + result);
      return result;
    } else {
      Log.d(TAG, "onInterceptTouchEvent: returned: false");
      return false;
    }
  }
}

import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  private username: String = "This is the userName";

  constructor() {
  }

  ngOnInit() {
  }

  isEmpty() {
    return !this.username.length;
  }

  resetUsername() {
    this.username = '';
  }
}

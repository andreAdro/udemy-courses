import {Component, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild('form', {static: true}) signupUpForm: NgForm;
  defaultSubscription = 'advanced';

  submitted = false;
  formData = {
    "email": "",
    "password": "",
    "subscription": ""
  };


  onSubmit(form: NgForm) {
    this.submitted = true;
    console.log(form);

    this.formData.email = this.signupUpForm.value.email;
    this.formData.password = this.signupUpForm.value.password;
    this.formData.subscription = this.signupUpForm.value.subscription;
  }
}

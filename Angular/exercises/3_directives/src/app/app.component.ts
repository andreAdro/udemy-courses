import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: [`
    .over5 {
      color: white;
    }
  `]
})
export class AppComponent {
  title = 'directives';
  show = false;
  numberOfClicks = 0;
  clicks: string[] = [];

  toggleShow() {
    this.show = !this.show;
    this.numberOfClicks++;
    this.clicks.push(String(this.clicks.length + 1));
    console.log(this.numberOfClicks);
  }

  isShown() {
    return this.show;
  }

  getBackGroundColor(clicked: string) {
    if (this.isOver4(clicked)) {
      return 'blue';
    }
  }

  isOver4(clicked: string) {
    for (let i = 0; i < this.clicks.length; i++) {
      const titleElement = this.clicks[i];
      if (clicked === titleElement && i > 3) {
        return true;
      }
    }
  }
}

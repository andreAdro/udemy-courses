import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Observable} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  signupForm: FormGroup;

  ngOnInit(): void {
    this.signupForm = new FormGroup({
      'projectName': new FormControl(null,
        [Validators.required],
        this.asyncForbiddenProjectNames.bind(this)
      ),
      'email': new FormControl(null,
        [Validators.required, Validators.email]),
      'status': new FormControl()
    })
  }

  onSubmit() {
    console.log(this.signupForm);
  }

  forbiddenProjectNames(control: FormControl): { [s: string]: boolean } {
    if (control.value === 'test') {
      return {'projectNameIsForbidden': true}
    }
    return null;
  }

  asyncForbiddenProjectNames(control: FormControl): Promise<any> | Observable<any> {
    return new Promise<any>((resolve, reject) => {
      setTimeout(() => {
        if (control.value === 'sup') {
          resolve({'projectNameSupIsForbidden': true})
        } else {
          resolve(null)
        }
      }, 1500)
    });
  }
}

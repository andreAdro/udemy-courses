import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @Output() evenNumbersEvent = new EventEmitter<number[]>();
  @Output() oddNumbersEvent = new EventEmitter<number[]>();

  evenNumbers: number[] = [];
  oddNumbers: number[] = [];

  onIntervalFired(firedNumber: number) {
    if (firedNumber % 2 === 0) {
      this.evenNumbers.push(firedNumber);
      this.evenNumbersEvent.emit(this.oddNumbers);
    } else {
      this.oddNumbers.push(firedNumber);
      this.oddNumbersEvent.emit(this.oddNumbers);
    }
  }
}

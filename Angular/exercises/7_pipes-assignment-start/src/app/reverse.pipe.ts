import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'reverse'
})
export class ReversePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    let strings = value.split('');
    strings.reverse();

    let reversedString = '';
    for (let val of strings) {
      reversedString += val;
    }
    return reversedString;
  }

}

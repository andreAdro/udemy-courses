import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    let propName: string = args[0];
    return value.sort((a, b) =>{
      if (a[propName] > b[propName]) {
        return 1;
      }
      return -1;
    });
  }

}

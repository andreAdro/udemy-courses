import {Injectable} from "@angular/core";
import {Post} from "./post.model";
import {HttpClient, HttpEventType, HttpHeaders, HttpParams} from "@angular/common/http";
import {catchError, map, tap} from "rxjs/operators";
import {Subject, throwError} from "rxjs";

@Injectable({providedIn: "root"})
export class PostsService {
  URL = 'https://ng-complete-guide-a3501.firebaseio.com/posts.json';
  error = new Subject<string>();

  constructor(private http: HttpClient) {
  }

  public createPost(post: Post) {
    this.http
      .post<{ name: string }>(
        this.URL,
        post,
        {
          observe: 'response'
        }
      )
      .subscribe(responseData => {
          console.log(responseData);
        }, error => {
          this.error.next(error.message);
        }
      );
  };

  fetchPosts() {
    let seachParams = new HttpParams();
    seachParams = seachParams.append('print', 'pretty');
    seachParams = seachParams.append('custom', 'key');

    return this.http.get<{ [key: string]: Post }>(
      this.URL,
      {
        headers: new HttpHeaders({"Custom": 'hello'}),
        params: seachParams,
        responseType: "json"
      })
      .pipe(map(responseData => {
        let postsArray: Post[] = [];
        for (let key in responseData) {
          if (responseData.hasOwnProperty(key)) {
            postsArray.push({...responseData[key], id: key})
          }
        }
        return postsArray;
      }), catchError(errorRes => {
        return throwError(errorRes);
      }));
  }

  deletePosts() {
    return this.http.delete(this.URL,
      {
        observe: 'events',
        responseType: "text"
      }).pipe(tap(
      event => {
        console.log(event);
        if (event.type === HttpEventType.Sent) {
          // ...
        }
        if (event.type === HttpEventType.Response) {
          console.log(event.body);
        }
      }
    ));
  }

}

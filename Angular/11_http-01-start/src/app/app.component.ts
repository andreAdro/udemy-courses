import {Component, OnDestroy, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Post} from "./post.model";
import {PostsService} from "./posts.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  URL = 'https://ng-complete-guide-a3501.firebaseio.com/posts.json';
  loadedPosts: Post[] = [];
  isFetching = false;
  error = null;
  private errorSub: Subscription;

  constructor(private http: HttpClient,
              private postService: PostsService) {
  }

  ngOnInit() {
    this.fetchPostsFromService();
    this.errorSub = this.postService.error.subscribe(errorMessage => {
      this.error = errorMessage;
    })
  }

  onCreatePost(postData: Post) {
    console.log(postData);
    // Send Http request
    this.postService.createPost(postData);
  }

  onFetchPosts() {
    // Send Http request
    this.fetchPostsFromService();
  }

  onClearPosts() {
    // Send Http request
    this.postService.deletePosts()
      .subscribe(() => {
        this.loadedPosts = [];
      })
  }

  fetchPostsFromService() {
    this.isFetching = true;
    this.postService.fetchPosts()
      .subscribe(posts => {
          this.isFetching = false;
          this.loadedPosts = posts;
        }, error => {
          this.isFetching = false;
          this.error = error.message;
          console.log(error);
        }
      );
  }

  ngOnDestroy(): void {
    this.errorSub.unsubscribe();
  }

  onHandleError() {
    this.error = null;
  }
}

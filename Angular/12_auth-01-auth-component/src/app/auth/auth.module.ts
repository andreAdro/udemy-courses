import {NgModule} from "@angular/core";
import {AuthComponent} from "./auth.component";
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {SharedModule} from "../shared/shared.module";
import {RecipesModule} from "../recipes/recipes.module";
import {CommonModule} from "@angular/common";

@NgModule({
  declarations: [
    AuthComponent
  ],
  imports: [
    RouterModule.forChild([
      {path: 'auth', component: AuthComponent}
    ]),
    FormsModule,
    CommonModule,
    SharedModule,
    RecipesModule
  ]
})

export class AuthModule {

}

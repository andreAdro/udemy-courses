import {Component, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild('f', {static: true}) signupUpForm: NgForm;
  defaultQuestion = 'pet';
  answer = '';
  genders = ['male', 'female'];
  user = {
    username: '',
    email: '',
    secretQuestion: '',
    answer: '',
    gender: ''
  };
  submitted = false;

  suggestUserName() {
    const suggestedName = 'Superuser';
    // this.signupUpForm.setValue({
    //   userData: {
    //     username: suggestedName,
    //     email: ''
    //   },
    //   secret :'pet',
    //   questionAnswer: '',
    //   gender: 'male'
    // });
    this.signupUpForm.form.patchValue({
      userData: {
        username: suggestedName
      }
    });
  }

  // onSubmit(signupUpForm: NgForm){
  // consoloe.log(signupUpForm);
  // }

  onSubmit() {
    this.submitted = true;
    console.log(this.signupUpForm);
    console.log(this.signupUpForm.value.userData.username);
    this.user.username = this.signupUpForm.value.userData.username;
    this.user.email = this.signupUpForm.value.userData.email;
    this.user.secretQuestion = this.signupUpForm.value.secret;
    this.user.answer = this.signupUpForm.value.questionAnswer;
    this.user.gender = this.signupUpForm.value.gender;

    this.signupUpForm.reset();
  }

}

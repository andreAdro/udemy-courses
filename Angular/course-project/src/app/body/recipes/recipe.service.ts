import {Recipe} from "./recipe.model";
import {Injectable} from "@angular/core";
import {Ingredient} from "../../shared/Ingredient.model";
import {ShoppingListService} from "../shopping-list/shopping-list.service";
import {Subject} from "rxjs";

@Injectable()
export class RecipeService {
  recipesChanged = new Subject<Recipe[]>();

  constructor(private shoppingListService: ShoppingListService) {
  }

  private recipes: Recipe[] = [];
  //   = [
  //   new Recipe('Areias de Sintra', 'Margarida\'s weakness', 'https://www.saborintenso.com/images/receitas/Areias-SI-1.jpg',
  //     [
  //       new Ingredient('Flour', 2),
  //       new Ingredient('Sugar', 3)
  //     ]),
  //   new Recipe('Outras Areias de Sintra', 'Margarida\'s weakness', 'https://www.saborintenso.com/images/receitas/Areias-SI-1.jpg',
  //     [
  //       new Ingredient('Flour', 4),
  //       new Ingredient('Sugar', 6)
  //     ]),
  // ];

  setRecipes(recipes: Recipe[]) {
    this.recipes = recipes;
    this.recipesChanged.next(this.recipes.slice());
  }

  getRecipes() {
    return this.recipes.slice();
  }

  getRecipe(index: number) {
    return this.recipes[index];
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.shoppingListService.addIngredients(ingredients);
  }

  addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
    this.recipesChanged.next(this.recipes.slice());
  }

  updateRecipe(index: number, recipe: Recipe) {
    this.recipes[index] = recipe;
    this.recipesChanged.next(this.recipes.slice());
  }

  deleteRecipe(index: number) {
    this.recipes.splice(index);
    this.recipesChanged.next(this.recipes.slice());
  }
}

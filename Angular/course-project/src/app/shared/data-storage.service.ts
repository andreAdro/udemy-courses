import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {RecipeService} from "../body/recipes/recipe.service";
import {Recipe} from "../body/recipes/recipe.model";
import {map, tap} from "rxjs/operators";

@Injectable({providedIn: "root"})
export class DataStorageService {
  URL = 'https://ng-cource-recipe-book-f3f26.firebaseio.com/recipes.json';

  constructor(private httpClient: HttpClient, private  recipeService: RecipeService) {
  }

  storeRecipes() {
    let recipes = this.recipeService.getRecipes();
    console.log(recipes);
    this.httpClient.put(this.URL, recipes)
      .subscribe(response => {
        console.log(response);
      });
  }

  fetchRecipes() {
    return this.httpClient.get<Recipe[]>(this.URL)
      .pipe(
        map(recipes => {
          return recipes.map(recipe => {
            return {...recipe, ingredients: recipe.ingredients ? recipe.ingredients : []};
          });
        }),
        tap(recipes => {
          this.recipeService.setRecipes(recipes);
        })
      )
  }
}

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RecipesComponent} from "./body/recipes/recipes.component";
import {ShoppingListComponent} from "./body/shopping-list/shopping-list.component";
import {RecipeStartComponent} from "./body/recipes/recipe-start/recipe-start.component";
import {RecipeDetailComponent} from "./body/recipes/recipe-detail/recipe-detail.component";
import {RecipeEditComponent} from "./body/recipes/recipe-edit/recipe-edit.component";
import {RecipesResolver} from "./body/recipes/recipes-resolver";

const routes: Routes = [
  {path: '', redirectTo: '/recipes', pathMatch: 'full'},
  {
    path: 'recipes', component: RecipesComponent,
    children: [
      {path: '', component: RecipeStartComponent},
      {path: 'new', component: RecipeEditComponent},
      {
        path: ':id',
        component: RecipeDetailComponent,
        resolve: [RecipesResolver]
      },
      {
        path: ':id/edit',
        component: RecipeEditComponent,
        resolve: [RecipesResolver]
      }
    ]
  },
  {path: 'shopping-list', component: ShoppingListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

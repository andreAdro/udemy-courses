import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {BodyComponent} from './body/body.component';
import {ShoppingListComponent} from './body/shopping-list/shopping-list.component';
import {RecipeListComponent} from './body/recipes/recipe-list/recipe-list.component';
import {RecipeItemComponent} from './body/recipes/recipe-list/recipe-item/recipe-item.component';
import {RecipeDetailComponent} from './body/recipes/recipe-detail/recipe-detail.component';
import {ShoppingEditComponent} from './body/shopping-list/shopping-edit/shopping-edit.component';
import {RecipesComponent} from './body/recipes/recipes.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DropdownDirective} from './shared/dropdown.directive';
import {ShoppingListService} from "./body/shopping-list/shopping-list.service";
import {RecipeStartComponent} from "./body/recipes/recipe-start/recipe-start.component";
import {RecipeEditComponent} from './body/recipes/recipe-edit/recipe-edit.component';
import {RecipeService} from "./body/recipes/recipe.service";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BodyComponent,
    ShoppingListComponent,
    RecipeListComponent,
    RecipeItemComponent,
    RecipeDetailComponent,
    ShoppingEditComponent,
    RecipesComponent,
    DropdownDirective,
    RecipeStartComponent,
    RecipeEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [ShoppingListService, RecipeService],
  bootstrap: [AppComponent]
})
export class AppModule {
}

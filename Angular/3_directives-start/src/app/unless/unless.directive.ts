import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appUnless]'
})
export class UnlessDirective {
  @Input() set appUnless(condition: boolean) {
    if (!condition) {
      this.viewContainterRef.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainterRef.clear();
    }
  }

  constructor(private templateRef: TemplateRef<any>, private viewContainterRef: ViewContainerRef) {
  }

}

export class AuthService {
  loggedIn = false;

  isAuthenticated() {
    const promise = new Promise(
      ((resolve, reject) =>
        setTimeout(() => resolve(this.loggedIn = true), 800))
    );
    return promise;
  }

  login() {
    this.loggedIn = true;
  }

  logOut() {
    this.loggedIn = false;
  }

}

package adro.springframework.didemo.controllers;

import adro.springframework.didemo.services.GreetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

@Controller
public class ConstructorInjectedController {

  private GreetingService greetingService;

  @Autowired
  public ConstructorInjectedController( GreetingService greetingService) {
    this.greetingService = greetingService;
  }
}

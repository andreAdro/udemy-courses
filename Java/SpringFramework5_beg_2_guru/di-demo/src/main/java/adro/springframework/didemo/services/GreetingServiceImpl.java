package adro.springframework.didemo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GreetingServiceImpl implements GreetingService {

  @Autowired
  public GreetingServiceImpl() {
  }

  @Override
  public String sayGreeting() {
    return "Greetings!";
  }
}

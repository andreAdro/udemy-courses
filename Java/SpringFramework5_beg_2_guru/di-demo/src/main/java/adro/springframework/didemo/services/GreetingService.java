package adro.springframework.didemo.services;

public interface GreetingService {

  String sayGreeting();
}

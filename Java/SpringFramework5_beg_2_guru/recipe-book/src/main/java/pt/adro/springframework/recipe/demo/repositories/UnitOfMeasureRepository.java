package pt.adro.springframework.recipe.demo.repositories;

import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import pt.adro.springframework.recipe.demo.domain.UnitOfMeasure;

/**
 * @author Adro @ 2020-01-04
 */
public interface UnitOfMeasureRepository extends CrudRepository<UnitOfMeasure, Long> {

  Optional<UnitOfMeasure> findByDescription(String description);
}

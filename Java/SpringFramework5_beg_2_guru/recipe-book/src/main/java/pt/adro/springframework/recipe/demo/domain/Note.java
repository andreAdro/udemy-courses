package pt.adro.springframework.recipe.demo.domain;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Adro @ 2020-01-04
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
public class Note extends AbstractRecipeEntity {

  private static final long serialVersionUID = 6014233318028390099L;

  @OneToOne
  private Recipe recipe;
  @Lob
  private String recipeNotes;

}

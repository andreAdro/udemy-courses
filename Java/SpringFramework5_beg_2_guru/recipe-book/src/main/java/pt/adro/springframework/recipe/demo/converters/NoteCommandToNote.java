package pt.adro.springframework.recipe.demo.converters;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pt.adro.springframework.recipe.demo.commands.NotesCommand;
import pt.adro.springframework.recipe.demo.domain.Note;

@Component
public class NoteCommandToNote implements Converter<NotesCommand, Note> {

  @Synchronized
  @Nullable
  @Override
  public Note convert(NotesCommand source) {
    if(source == null) {
      return null;
    }

    final Note notes = new Note();
    notes.setId(source.getId());
    notes.setRecipeNotes(source.getRecipeNotes());
    return notes;
  }
}

package pt.adro.springframework.recipe.demo.repositories;

import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import pt.adro.springframework.recipe.demo.domain.Category;

/**
 * @author Adro @ 2020-01-04
 */
public interface CategoryRepository extends CrudRepository<Category, Long> {

  Optional<Category> findByDescription(String description);

}

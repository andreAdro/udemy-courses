package pt.adro.springframework.recipe.demo.converters;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pt.adro.springframework.recipe.demo.commands.UnitOfMeasureCommand;
import pt.adro.springframework.recipe.demo.domain.UnitOfMeasure;

@Component
public class UnitOfMeasureToUnitOfMeasureCommand implements
    Converter<UnitOfMeasure, UnitOfMeasureCommand> {

  @Synchronized
  @Nullable
  @Override
  public UnitOfMeasureCommand convert(UnitOfMeasure unitOfMeasure) {

    if (unitOfMeasure != null) {
      final UnitOfMeasureCommand uomc = new UnitOfMeasureCommand();
      uomc.setId(unitOfMeasure.getId());
      uomc.setDescription(unitOfMeasure.getDescription());
      return uomc;
    }
    return null;
  }
}

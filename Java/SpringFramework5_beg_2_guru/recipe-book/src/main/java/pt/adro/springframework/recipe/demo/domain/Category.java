package pt.adro.springframework.recipe.demo.domain;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Adro @ 2020-01-04
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
public class Category extends AbstractRecipeEntity {

  private static final long serialVersionUID = -5169081803295288187L;
  private String description;

  @ManyToMany(mappedBy = "categories")
  private Set<Recipe> recipes;

}

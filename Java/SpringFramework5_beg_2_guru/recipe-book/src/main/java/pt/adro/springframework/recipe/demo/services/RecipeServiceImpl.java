package pt.adro.springframework.recipe.demo.services;

import java.util.HashSet;
import java.util.Set;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.adro.springframework.recipe.demo.commands.RecipeCommand;
import pt.adro.springframework.recipe.demo.converters.RecipeCommandToRecipe;
import pt.adro.springframework.recipe.demo.converters.RecipeToRecipeCommand;
import pt.adro.springframework.recipe.demo.domain.Recipe;
import pt.adro.springframework.recipe.demo.repositories.RecipeRepository;

/**
 * @author Adro @ 2020-01-04
 */
@Slf4j
@Service
public class RecipeServiceImpl implements RecipeService {

  private RecipeRepository recipeRepository;
  private RecipeCommandToRecipe recipeCommandToRecipe;
  private RecipeToRecipeCommand recipeToRecipeCommand;

  @Autowired
  public RecipeServiceImpl(RecipeRepository recipeRepository,
      RecipeCommandToRecipe recipeCommandToRecipe,
      RecipeToRecipeCommand recipeToRecipeCommand) {
    this.recipeRepository = recipeRepository;
    this.recipeCommandToRecipe = recipeCommandToRecipe;
    this.recipeToRecipeCommand = recipeToRecipeCommand;
  }

  @Override
  public Set<Recipe> getRecipes() {
    Set<Recipe> recipes = new HashSet<>();
    recipeRepository.findAll().forEach(recipes::add);
    return recipes;
  }

  @Override
  public Recipe findById(Long l) {
    return recipeRepository.findById(l).orElseThrow(RuntimeException::new);
  }

  @Override
  @Transactional
  public RecipeCommand findCommandById(long anyLong) {
    return this.recipeToRecipeCommand.convert(this.findById(anyLong));
  }

  @Override
  @Transactional
  public RecipeCommand saveRecipeCommand(RecipeCommand command) {
    Recipe detachedRecipe = recipeCommandToRecipe.convert(command);
    Recipe savedRecipe = recipeRepository.save(detachedRecipe);
    log.debug("Saved RecipeId:" + savedRecipe.getId());

    return recipeToRecipeCommand.convert(savedRecipe);
  }
}

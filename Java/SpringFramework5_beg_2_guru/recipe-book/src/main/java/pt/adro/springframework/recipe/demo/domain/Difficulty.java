package pt.adro.springframework.recipe.demo.domain;

/**
 * @author Adro @ 2020-01-04
 */
public enum Difficulty {

  EASY, MEDIUM, MODERATE, HARD;
}

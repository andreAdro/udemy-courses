package pt.adro.springframework.recipe.demo.services;

import java.util.Set;
import pt.adro.springframework.recipe.demo.commands.RecipeCommand;
import pt.adro.springframework.recipe.demo.domain.Recipe;

/**
 * @author Adro @ 2020-01-04
 */
public interface RecipeService {

  Set<Recipe> getRecipes();

  Recipe findById(Long l);

  RecipeCommand findCommandById(long anyLong);

  RecipeCommand saveRecipeCommand(RecipeCommand command);
}

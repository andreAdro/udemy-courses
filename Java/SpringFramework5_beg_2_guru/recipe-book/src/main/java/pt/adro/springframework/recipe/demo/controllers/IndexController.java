package pt.adro.springframework.recipe.demo.controllers;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import pt.adro.springframework.recipe.demo.domain.Category;
import pt.adro.springframework.recipe.demo.domain.UnitOfMeasure;
import pt.adro.springframework.recipe.demo.services.RecipeService;

/**
 * Created by: adroa Date: 2019-10-02
 */
@Controller
public class IndexController {

  private RecipeService recipeService;

  @Autowired
  public IndexController(RecipeService recipeService) {
    this.recipeService = recipeService;
  }

  @GetMapping({"", "/", "/index"})
  public String getIndexPage(Model model) {

    model.addAttribute("recipes", recipeService.getRecipes());

    return "index";
  }
}

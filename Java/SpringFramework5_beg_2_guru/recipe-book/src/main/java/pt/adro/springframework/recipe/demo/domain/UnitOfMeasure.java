package pt.adro.springframework.recipe.demo.domain;

import javax.persistence.Entity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Adro @ 2020-01-04
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
public class UnitOfMeasure extends AbstractRecipeEntity {

  private static final long serialVersionUID = -1598488378040101857L;
  private String description;

}

package pt.adro.springframework.recipe.demo.repositories;

import org.springframework.data.repository.CrudRepository;
import pt.adro.springframework.recipe.demo.domain.Recipe;

/**
 * @author Adro @ 2020-01-04
 */
public interface RecipeRepository extends CrudRepository<Recipe, Long> {

}

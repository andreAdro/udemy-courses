package pt.adro.springframework.recipe.demo.converters;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pt.adro.springframework.recipe.demo.commands.NotesCommand;
import pt.adro.springframework.recipe.demo.domain.Note;

@Component
public class NoteToNoteCommand implements Converter<Note, NotesCommand> {

  @Synchronized
  @Nullable
  @Override
  public NotesCommand convert(Note source) {
    if (source == null) {
      return null;
    }

    final NotesCommand notesCommand = new NotesCommand();
    notesCommand.setId(source.getId());
    notesCommand.setRecipeNotes(source.getRecipeNotes());
    return notesCommand;
  }
}

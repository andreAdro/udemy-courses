package pt.adro.springframework.recipe.demo.controllers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.HashSet;
import java.util.Set;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;
import pt.adro.springframework.recipe.demo.domain.Recipe;
import pt.adro.springframework.recipe.demo.services.RecipeService;

public class IndexControllerTest {

  @Mock
  RecipeService recipeService;

  @Mock
  Model model;

  IndexController indexController;

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    indexController = new IndexController(recipeService);
  }


  @Test
  public void testMockMVC() throws Exception {
    MockMvc mockMvc = MockMvcBuilders.standaloneSetup(indexController).build();

    mockMvc.perform(MockMvcRequestBuilders.get("/"))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(view().name("index"));
  }

  @Test
  public void getIndexPage() {
    // given
    Set<Recipe> recipes = new HashSet<>();
    Recipe firstRecipe = new Recipe();
    firstRecipe.setId(1L);
    recipes.add(firstRecipe);
    Recipe secondRecipe = new Recipe();
    secondRecipe.setId(2L);
    recipes.add(secondRecipe);

    // when
    Mockito.when(recipeService.getRecipes()).thenReturn(recipes);

    ArgumentCaptor<Set<Recipe>> argumentCaptor = ArgumentCaptor.forClass(Set.class);

    String viewName = indexController.getIndexPage(model);

    // then
    Assert.assertEquals("index", viewName);
    Mockito.verify(recipeService, Mockito.times(1)).getRecipes();
    Mockito.verify(model, Mockito.times(1))
        .addAttribute(Mockito.eq("recipes"), argumentCaptor.capture());
    Set<Recipe> setInController = argumentCaptor.getValue();
    Assert.assertEquals(2, setInController.size());
  }
}
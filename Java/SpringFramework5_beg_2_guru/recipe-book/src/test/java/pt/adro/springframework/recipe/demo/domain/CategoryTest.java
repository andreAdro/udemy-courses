package pt.adro.springframework.recipe.demo.domain;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CategoryTest {

  Category category;

  @Before
  public void setUp() {
    category = new Category();
  }

  @Test
  public void getDescription() {
    String descriptionString = "description string";
    category.setDescription(descriptionString);

    Assert.assertEquals(descriptionString, category.getDescription());
  }

  @Test
  public void getRecipes() {
  }
}
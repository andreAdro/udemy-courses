package pt.adro.springframework.joke.jokeapp.controllers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pt.adro.springframework.joke.jokeapp.services.JokeService;

/** Created by: adroa Date: 2019-09-14 */
@Controller
public class JokeController {

  private JokeService jokeService;

  @Autowired
  public JokeController(JokeService jokeService) {
    this.jokeService = jokeService;
  }

  @RequestMapping({"/", ""})
  public String showJoke(Model model) {
    model.addAttribute("joke", jokeService.getJoke());
    List<String> list = new ArrayList<>();
    Set<String> set = new HashSet<>();
    set.stream().filter()
    list.stream().collect(Collectors.toList())
    return "chucknorris";
  }
}

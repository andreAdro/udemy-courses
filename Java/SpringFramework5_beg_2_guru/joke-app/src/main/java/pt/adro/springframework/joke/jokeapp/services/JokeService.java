package pt.adro.springframework.joke.jokeapp.services;

/**
 * Created by: adroa Date:  2019-09-14
 */
public interface JokeService {

  String getJoke();


}

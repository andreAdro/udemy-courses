package pt.adro.springframework.spring5webapp.repositories;

import org.springframework.data.repository.CrudRepository;
import pt.adro.springframework.spring5webapp.model.Author;

public interface AuthorRepository extends CrudRepository<Author, Long> {}

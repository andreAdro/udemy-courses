package pt.adro.springframework.spring5webapp.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@ToString(callSuper = true)
@Entity
public class Publisher extends JPAEntity {

  private String name;
  private String address;

  @OneToOne
  private Book book;

  public Publisher(String name, String address) {
    this.name = name;
    this.address = address;
  }

}

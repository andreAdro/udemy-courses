package pt.adro.springframework.spring5webapp.bootstrap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import pt.adro.springframework.spring5webapp.model.Author;
import pt.adro.springframework.spring5webapp.model.Book;
import pt.adro.springframework.spring5webapp.model.Publisher;
import pt.adro.springframework.spring5webapp.repositories.AuthorRepository;
import pt.adro.springframework.spring5webapp.repositories.BookRepository;
import pt.adro.springframework.spring5webapp.repositories.PublisherRepository;

@Component
public class DevBootStrap implements ApplicationListener<ContextRefreshedEvent> {

  private AuthorRepository authorRepository;
  private BookRepository bookRepository;
  private PublisherRepository publisherRepository;

  @Autowired
  public DevBootStrap(
      AuthorRepository authorRepository,
      BookRepository bookRepository,
      PublisherRepository publisherRepository) {
    this.authorRepository = authorRepository;
    this.bookRepository = bookRepository;
    this.publisherRepository = publisherRepository;
  }

  @Override
  public void onApplicationEvent(ContextRefreshedEvent event) {
    initData();
  }

  private void initData() {
    Publisher publisher = new Publisher("Publisher Name", "Publisher Address");
    this.publisherRepository.save(publisher);

    Author eric = new Author("Eric", "Evans");
    Book ddd = new Book("Domain Drived Design", "1234", publisher);
    eric.getBooks().add(ddd);
    ddd.getAuthors().add(eric);

    this.authorRepository.save(eric);
    this.bookRepository.save(ddd);

    Author rod = new Author("Rod", "Johnson");
    Book noEJB = new Book("J2EE Development without EJB", "23444", publisher);
    rod.getBooks().add(noEJB);
    noEJB.getAuthors().add(rod);

    this.authorRepository.save(rod);
    this.bookRepository.save(noEJB);
  }
}

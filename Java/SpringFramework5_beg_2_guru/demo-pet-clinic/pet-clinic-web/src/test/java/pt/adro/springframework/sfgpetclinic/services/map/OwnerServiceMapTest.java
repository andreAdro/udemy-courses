package pt.adro.springframework.sfgpetclinic.services.map;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pt.adro.springframework.sfgpetclinic.model.Owner;
import pt.adro.springframework.sfgpetclinic.services.PetService;

class OwnerServiceMapTest {

  OwnerServiceMap ownerServiceMap;

  @BeforeEach
  void setUp() {
    ownerServiceMap = new OwnerServiceMap(new PetTypeMapService(), new PetServiceMap());
    ownerServiceMap.save(Owner.builder().build());
  }

  @Test
  void save() {
  }

  @Test
  void findByLastName() {
  }

  @Test
  void findAll() {
  }

  @Test
  void findById() {
  }

  @Test
  void deleteById() {
  }

  @Test
  void delete() {
  }
}
package pt.adro.springframework.sfgpetclinic.services.springdatajpa;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pt.adro.springframework.sfgpetclinic.model.Owner;
import pt.adro.springframework.sfgpetclinic.repositories.OwnerRepository;
import pt.adro.springframework.sfgpetclinic.repositories.PetRepository;
import pt.adro.springframework.sfgpetclinic.repositories.PetTypeRepository;

@ExtendWith(MockitoExtension.class)
public class OwnerSDJpaServiceTest {

  public static final String LAST_NAME = "Smith";
  @Mock
  OwnerRepository ownerRepository;

  @Mock
  PetRepository petRepository;

  @Mock
  PetTypeRepository petTypeRepository;

  @InjectMocks
  OwnerSDJpaService ownerSDJpaService;

  @BeforeEach
  void setUp() {
  }

  @Test
  void findByLastName() {
    Owner returnOwner = Owner.builder().id(1L).lastName(LAST_NAME).build();

    when(ownerSDJpaService.findByLastName(anyString())).thenReturn(returnOwner);

    Owner smith = ownerSDJpaService.findByLastName(LAST_NAME);

//    Assert.assertEquals(LAST_NAME, smith.getLastName());
    verify(ownerRepository).findByLastName(any());
  }

  @Test
  void testFindAll() {
  }

  @Test
  void testFindById() {
  }

  @Test
  void testSave() {
  }

  @Test
  void testDelete() {
  }

  @Test
  void testDeleteById() {
  }


}
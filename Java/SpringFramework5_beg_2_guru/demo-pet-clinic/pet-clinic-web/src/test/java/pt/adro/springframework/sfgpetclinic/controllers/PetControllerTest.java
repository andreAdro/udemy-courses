package pt.adro.springframework.sfgpetclinic.controllers;

import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pt.adro.springframework.sfgpetclinic.model.Owner;
import pt.adro.springframework.sfgpetclinic.model.Pet;
import pt.adro.springframework.sfgpetclinic.model.PetType;
import pt.adro.springframework.sfgpetclinic.services.OwnerService;
import pt.adro.springframework.sfgpetclinic.services.PetService;
import pt.adro.springframework.sfgpetclinic.services.PetTypeService;

@ExtendWith(MockitoExtension.class)
class PetControllerTest {

  @Mock
  PetService petService;

  @Mock
  OwnerService ownerService;

  @Mock
  PetTypeService petTypeService;

  @InjectMocks
  PetController petController;

  MockMvc mockMvc;

  Owner owner;

  Set<PetType> petTypes;

  @BeforeEach
  void setUp() {
    owner = Owner.builder().id(1L).build();

    petTypes = new HashSet<>();
    petTypes.add(PetType.builder().id(1L).name("Dog").build());
    petTypes.add(PetType.builder().id(2L).name("Cat").build());

    mockMvc = MockMvcBuilders
        .standaloneSetup(petController)
        .build();
  }

  @Test
  void initCreationForm() throws Exception {
    Mockito.when(ownerService.findById(Mockito.anyLong())).thenReturn(owner);
    Mockito.when(petTypeService.findAll()).thenReturn(petTypes);

    mockMvc.perform(MockMvcRequestBuilders.get("/owners/1/pets/new"))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.model().attributeExists("owner"))
        .andExpect(MockMvcResultMatchers.model().attributeExists("pet"))
        .andExpect(MockMvcResultMatchers.view().name("pets/createOrUpdatePetForm"));
  }

  @Test
  void processCreationForm() throws Exception {
    Mockito.when(ownerService.findById(Mockito.anyLong())).thenReturn(owner);
    Mockito.when(petTypeService.findAll()).thenReturn(petTypes);

    mockMvc.perform(MockMvcRequestBuilders.post("/owners/1/pets/new"))
        .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
        .andExpect(MockMvcResultMatchers.view().name("redirect:/owners/1"));

//    Mockito.verify(petService.save(Mockito.any()));
  }

  @Test
  void initUpdateForm() throws Exception {
    Mockito.when(ownerService.findById(Mockito.anyLong())).thenReturn(owner);
    Mockito.when(petTypeService.findAll()).thenReturn(petTypes);
    Mockito.when(petService.findById(Mockito.anyLong())).thenReturn(Pet.builder().id(2L).build());

    mockMvc.perform(MockMvcRequestBuilders.get("/owners/1/pets/2/edit"))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.model().attributeExists("owner"))
        .andExpect(MockMvcResultMatchers.model().attributeExists("pet"))
        .andExpect(MockMvcResultMatchers.view().name("pets/createOrUpdatePetForm"));

//    Mockito.verify(petService.save(Mockito.any()));
  }

  @Test
  void processUpdateForm() throws Exception {
    Mockito.when(ownerService.findById(Mockito.anyLong())).thenReturn(owner);
    Mockito.when(petTypeService.findAll()).thenReturn(petTypes);

    mockMvc.perform(MockMvcRequestBuilders.post("/owners/1/pets/2/edit"))
        .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
        .andExpect(MockMvcResultMatchers.view().name("redirect:/owners/1"));

//    Mockito.verify(petService.save(Mockito.any()));
  }


}
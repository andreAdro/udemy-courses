package pt.adro.springframework.sfgpetclinic.controllers;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import pt.adro.springframework.sfgpetclinic.model.Pet;
import pt.adro.springframework.sfgpetclinic.model.Visit;
import pt.adro.springframework.sfgpetclinic.services.PetService;
import pt.adro.springframework.sfgpetclinic.services.VisitService;

/**
 * @author Adro @ 3/6/2020
 */
@Controller
public class VisitController {

  private final VisitService visitService;
  private final PetService petService;

  @Autowired
  public VisitController(VisitService visitService,
      PetService petService) {
    this.visitService = visitService;
    this.petService = petService;
  }

  @InitBinder
  public void setAllowedFields(WebDataBinder dataBinder) {
    dataBinder.setDisallowedFields("id");
  }

  @ModelAttribute("visit")
  public Visit loadPetWithVisit(@PathVariable Long petId, Model model) {
    Pet pet = this.petService.findById(petId);
    model.addAttribute("pet", pet);
    Visit visit = new Visit();
    pet.getVisits().add(visit);
    visit.setPet(pet);
    return visit;
  }

  @GetMapping("/owners/*/pets/{petId}/visits/new")
  public String initNewVisitForm(@PathVariable Long petId, Model model) {
    return "pets/createOrUpdateVisitForm";
  }

  @PostMapping("/owners/{ownerId}/pets/{petId}/visits/new")
  public String processNewVisitForm(@Valid Visit visit, BindingResult result) {
    if (result.hasErrors()) {
      return "pets/createOrUpdateVisitForm";
    } else {
      this.visitService.save(visit);
      return "redirect:/owners/{ownerId}";
    }
  }

}

package pt.adro.springframework.sfgpetclinic.formatters;

import java.text.ParseException;
import java.util.Locale;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;
import pt.adro.springframework.sfgpetclinic.model.PetType;
import pt.adro.springframework.sfgpetclinic.services.PetTypeService;

/**
 * @author Adro @ 3/6/2020
 */
@Component
public class PetTypeFormatter implements Formatter<PetType> {

  private final PetTypeService petTypeService;

  @Autowired
  public PetTypeFormatter(PetTypeService petTypeService) {
    this.petTypeService = petTypeService;
  }

  @Override
  public String print(PetType object, Locale locale) {
    return object.getName();
  }

  @Override
  public PetType parse(String text, Locale locale) throws ParseException {
    Set<PetType> types = this.petTypeService.findAll();
    for (PetType type : types) {
      if (type.getName().equals(text)) {
        return type;
      }
    }
    throw new ParseException("type not found: " + text, 0);
  }
}

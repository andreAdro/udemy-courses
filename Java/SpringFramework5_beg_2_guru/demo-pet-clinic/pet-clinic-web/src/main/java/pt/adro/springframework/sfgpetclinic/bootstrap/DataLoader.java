package pt.adro.springframework.sfgpetclinic.bootstrap;

import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pt.adro.springframework.sfgpetclinic.model.Owner;
import pt.adro.springframework.sfgpetclinic.model.Pet;
import pt.adro.springframework.sfgpetclinic.model.PetType;
import pt.adro.springframework.sfgpetclinic.model.Speciality;
import pt.adro.springframework.sfgpetclinic.model.Vet;
import pt.adro.springframework.sfgpetclinic.model.Visit;
import pt.adro.springframework.sfgpetclinic.services.OwnerService;
import pt.adro.springframework.sfgpetclinic.services.PetTypeService;
import pt.adro.springframework.sfgpetclinic.services.SpecialityService;
import pt.adro.springframework.sfgpetclinic.services.VetService;
import pt.adro.springframework.sfgpetclinic.services.VisitService;

/** Created by: adroa Date: 2019-09-14 */
@Component
public class DataLoader implements CommandLineRunner {

  private final OwnerService ownerService;
  private final VetService vetService;
  private final PetTypeService petTypeService;
  private final SpecialityService specialityService;
  private final VisitService visitService;

  @Autowired
  public DataLoader(
      OwnerService ownerService,
      VetService vetService,
      PetTypeService petTypeService,
      SpecialityService specialityService,
      VisitService visitService) {
    this.ownerService = ownerService;
    this.vetService = vetService;
    this.petTypeService = petTypeService;
    this.specialityService = specialityService;
    this.visitService = visitService;
  }

  @Override
  public void run(String... args) throws Exception {
    int count = petTypeService.findAll().size();
    if (count == 0) {
      loadData();
    }
  }

  private void loadData() {
    PetType dog = new PetType();
    dog.setName("Dog");
    PetType savedDogType = petTypeService.save(dog);

    PetType cat = new PetType();
    cat.setName("Cat");
    PetType savedCatType = petTypeService.save(cat);

    Speciality radiology = new Speciality();
    radiology.setDescription("Radiology");
    Speciality savedRadiology = specialityService.save(radiology);

    Speciality surgery = new Speciality();
    surgery.setDescription("Surgery");
    Speciality savedSurgery = specialityService.save(surgery);

    Speciality dentistry = new Speciality();
    dentistry.setDescription("Dentistry");
    Speciality savedDentistry = specialityService.save(dentistry);

    Owner owner = new Owner();
    owner.setFirstName("Michael");
    owner.setLastName("Weston");
    owner.setAddress("This is an address.");
    owner.setCity("This is a city.");
    owner.setTelephone("This is a telephone.");

    Pet mikesPet = new Pet();
    mikesPet.setPetType(savedDogType);
    mikesPet.setOwner(owner);
    mikesPet.setName("Rosco");
    mikesPet.setBirthDate(LocalDate.now());
    owner.getPets().add(mikesPet);

    ownerService.save(owner);

    Owner owner1 = new Owner();
    owner1.setFirstName("Fiona");
    owner1.setLastName("Gienanne");
    owner1.setAddress("This is an address.");
    owner1.setCity("This is a city.");
    owner1.setTelephone("This is a telephone.");

    Pet fionasCat = new Pet();
    fionasCat.setName("Just Cat");
    fionasCat.setOwner(owner1);
    fionasCat.setBirthDate(LocalDate.now());
    fionasCat.setPetType(savedCatType);
    owner1.getPets().add(fionasCat);

    ownerService.save(owner1);
    Visit catVisit = new Visit();
    catVisit.setPet(fionasCat);
    catVisit.setDate(LocalDate.now());
    catVisit.setDescription("Sneezy Kitty");
    visitService.save(catVisit);

    System.out.println("Loaded Owners...");

    Vet vet = new Vet();
    vet.setFirstName("Sam");
    vet.setLastName("Axe");
    vet.getSpecialities().add(savedRadiology);

    vetService.save(vet);

    Vet vet1 = new Vet();
    vet1.setFirstName("Jessie");
    vet1.setLastName("Porter");
    vet1.getSpecialities().add(savedSurgery);

    vetService.save(vet1);

    System.out.println("Loaded Vets...");
  }
}

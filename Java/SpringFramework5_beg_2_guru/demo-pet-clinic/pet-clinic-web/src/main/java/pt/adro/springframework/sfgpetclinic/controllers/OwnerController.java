package pt.adro.springframework.sfgpetclinic.controllers;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import pt.adro.springframework.sfgpetclinic.model.Owner;
import pt.adro.springframework.sfgpetclinic.services.OwnerService;

/**
 * Created by: adroa Date: 2019-09-14
 */
@RequestMapping("/owners")
@Controller
public class OwnerController {

  private static final String VIEW_OWNER_CREATE_OR_UPDATE_FORM = "owners/createOrUpdateOwnerForm";
  private final OwnerService ownerService;

  @Autowired
  public OwnerController(OwnerService ownerService) {
    this.ownerService = ownerService;
  }

  @InitBinder
  public void getAllowedFields(WebDataBinder dataBinder) {
    dataBinder.setDisallowedFields("id");
  }

//  @GetMapping({"", "/", "/index", "/index.html"})
//  public String listOwners(Model model) {
//    model.addAttribute("owners", this.ownerService.findAll());
//    return "owners/index";
//  }

  @GetMapping({"/find"})
  public String findOwners(Model model) {
    model.addAttribute("owner", Owner.builder().build());
    return "owners/findOwners";
  }

  @GetMapping
  public String processFindForm(Owner owner, BindingResult result, Model model) {
    if (null == owner.getLastName()) {
      owner.setLastName("");
    }
    List<Owner> results = this.ownerService.findAllByLastNameLike(owner.getLastName());
    if (results.isEmpty()) {
      result.rejectValue("lastName", "notFound", "not found");
      return "owners/findOwners";
    } else if (1 == results.size()) {
      owner = results.get(0);
      return "redirect:/owners/" + owner.getId();
    } else {
      model.addAttribute("selections", results);
      return "owners/ownersList";
    }
  }

  @GetMapping("/{ownerId}")
  public ModelAndView showOwner(@PathVariable("ownerId") Long ownerId) {
    ModelAndView mav = new ModelAndView("owners/ownerDetails");
    mav.addObject(this.ownerService.findById(ownerId));
    return mav;
  }

  @GetMapping("/new")
  public String initCreationForm(Model model) {
    model.addAttribute("owner", Owner.builder().build());
    return VIEW_OWNER_CREATE_OR_UPDATE_FORM;
  }

  @PostMapping("/new")
  public String processCreationForm(@Valid Owner owner, BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      return VIEW_OWNER_CREATE_OR_UPDATE_FORM;
    } else {
      Owner savedOwner = this.ownerService.save(owner);
      return "redirect:/owners/" + savedOwner.getId();
    }
  }

  @GetMapping("{ownerId}/edit")
  public String initUpdateOwnerForm(@PathVariable("ownerId") Long ownerId, Model model) {
    model.addAttribute(this.ownerService.findById(ownerId));
    return VIEW_OWNER_CREATE_OR_UPDATE_FORM;
  }

  @PostMapping("{ownerId}/edit")
  public String processUpdatefoundForm(@Valid Owner owner, BindingResult result,
      @PathVariable("ownerId") Long ownerId) {
    if (result.hasErrors()) {
      return VIEW_OWNER_CREATE_OR_UPDATE_FORM;
    } else {
      owner.setId(ownerId);
      Owner savedOwner = this.ownerService.save(owner);
      return "redirect:/owners/" + savedOwner.getId();
    }
  }
}

package pt.adro.springframework.sfgpetclinic.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/** Created by: adroa Date: 2019-09-14 */
@Controller
public class IndexController {

  @RequestMapping({"", "/", "index", "index.html"})
  public String index() {
    return "index";
  }

  @RequestMapping({"oups"})
  public String oupsHandler() {
    return "not-implemented";
  }
}

package pt.adro.springframework.sfgpetclinic.services.map;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pt.adro.springframework.sfgpetclinic.model.PetType;
import pt.adro.springframework.sfgpetclinic.services.PetTypeService;

/** Created by: adroa Date: 2019-10-03 */
@Service
@Profile({"default", "map"})
public class PetTypeMapService extends AbstractMapService<PetType, Long>
    implements PetTypeService {}

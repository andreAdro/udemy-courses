package pt.adro.springframework.sfgpetclinic.services.map;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pt.adro.springframework.sfgpetclinic.model.Visit;
import pt.adro.springframework.sfgpetclinic.services.VisitService;

/**
 * @author Adro @ 2020-01-12
 */
@Service
@Profile({"default", "map"})
public class VisitMapService extends AbstractMapService<Visit, Long> implements VisitService {

  @Override
  public Visit save(Visit object) {
    if (object.getPet() == null || object.getPet().getOwner() == null
        || object.getPet().getId() == null || object.getPet().getOwner().getId() == null){
      throw new RuntimeException("Invalid visit");
    }
    return super.save(object);
  }
}

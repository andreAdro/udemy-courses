package pt.adro.springframework.sfgpetclinic.services.map;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pt.adro.springframework.sfgpetclinic.model.Speciality;
import pt.adro.springframework.sfgpetclinic.services.SpecialityService;

/** Created by: adroa Date: 2019-10-03 */
@Service
@Profile({"default", "map"})
public class SpecialityMapService extends AbstractMapService<Speciality, Long>
    implements SpecialityService {}

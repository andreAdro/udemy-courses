package pt.adro.springframework.sfgpetclinic.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.adro.springframework.sfgpetclinic.model.PetType;

/**
 * @author Adro @ 2020-01-08
 */
public interface PetTypeRepository extends JpaRepository<PetType, Long> {

}

package pt.adro.springframework.sfgpetclinic.services;

import java.util.Set;

/** Created by: adroa Date: 2019-09-14 */
public interface CrudService<T, ID> {

  Set<T> findAll();

  T findById(ID id);

  T save(T object);

  void delete(T object);

  void deleteById(ID id);
}

package pt.adro.springframework.sfgpetclinic.model;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by: adroa Date: 2019-10-03
 */
@Getter
@Setter
@Entity(name = "VISITS")
public class Visit extends BaseEntity {

  private static final long serialVersionUID = 2732184456377194184L;

  @Column(name = "DATE")
  private LocalDate date;

  @Column(name = "DESCRIPTION")
  private String description;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "PET_ID")
  private Pet pet;
}

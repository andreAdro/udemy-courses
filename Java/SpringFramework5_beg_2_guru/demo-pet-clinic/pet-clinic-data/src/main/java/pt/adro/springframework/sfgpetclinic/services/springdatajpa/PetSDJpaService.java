package pt.adro.springframework.sfgpetclinic.services.springdatajpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pt.adro.springframework.sfgpetclinic.model.Pet;
import pt.adro.springframework.sfgpetclinic.repositories.PetRepository;
import pt.adro.springframework.sfgpetclinic.services.PetService;

/**
 * @author Adro @ 2020-01-08
 */
@Service
@Profile("springdatajpa")
public class PetSDJpaService extends AbstractCRUDService<Pet, Long> implements PetService {

  private PetRepository petRepository;

  @Autowired
  public PetSDJpaService(PetRepository petRepository) {
    super(petRepository);
    this.petRepository = petRepository;
  }
}

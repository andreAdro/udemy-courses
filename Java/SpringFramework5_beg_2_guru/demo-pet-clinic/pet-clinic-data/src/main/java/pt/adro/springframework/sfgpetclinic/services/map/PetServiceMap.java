package pt.adro.springframework.sfgpetclinic.services.map;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pt.adro.springframework.sfgpetclinic.model.Pet;
import pt.adro.springframework.sfgpetclinic.services.CrudService;
import pt.adro.springframework.sfgpetclinic.services.PetService;

/**
 * Created by: adroa Date: 2019-09-14
 */
@Service
@Profile({"default", "map"})
public class PetServiceMap extends AbstractMapService<Pet, Long>
    implements CrudService<Pet, Long>, PetService {

}

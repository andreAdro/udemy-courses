package pt.adro.springframework.sfgpetclinic.services.springdatajpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pt.adro.springframework.sfgpetclinic.model.PetType;
import pt.adro.springframework.sfgpetclinic.repositories.PetTypeRepository;
import pt.adro.springframework.sfgpetclinic.services.PetTypeService;

/**
 * @author Adro @ 2020-01-08
 */
@Service
@Profile("springdatajpa")
public class PetTypeSDJpaService extends AbstractCRUDService<PetType, Long> implements
    PetTypeService {

  private PetTypeRepository petTypeRepository;

  @Autowired
  public PetTypeSDJpaService(PetTypeRepository petTypeRepository) {
    super(petTypeRepository);
    this.petTypeRepository = petTypeRepository;
  }
}

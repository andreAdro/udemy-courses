package pt.adro.springframework.sfgpetclinic.model;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.util.CollectionUtils;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "PETS")
public class Pet extends BaseEntity {

  private static final long serialVersionUID = -6331554330706969331L;

  @Column(name = "NAME")
  private String name;

  @ManyToOne
  @JoinColumn(name = "PET_TYPE_ID")
  private PetType petType;

  @ManyToOne
  @JoinColumn(name = "OWNER_ID")
  private Owner owner;

  @Column(name = "BIRTH_DATE")
  private LocalDate birthDate;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "pet")
  private Set<Visit> visits = new HashSet<>();

  @Builder
  public Pet(Long id, String name, PetType petType, Owner owner, LocalDate birthDate, Set<Visit> visits) {
    super(id);
    this.name = name;
    this.petType = petType;
    this.owner = owner;
    this.birthDate = birthDate;

    if (!CollectionUtils.isEmpty(visits) ) {
      this.visits = visits;
    }
  }
}

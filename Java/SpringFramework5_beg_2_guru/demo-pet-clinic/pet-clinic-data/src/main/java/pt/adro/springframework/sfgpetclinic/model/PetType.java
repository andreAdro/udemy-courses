package pt.adro.springframework.sfgpetclinic.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "PET_TYPES")
public class PetType extends BaseEntity {

  private static final long serialVersionUID = -4449321868189149379L;

  @Column(name = "NAME")
  private String name;

  @Builder
  public PetType(Long id, String name) {
    super(id);
    this.name = name;
  }

  @Override
  public String toString() {
    return name;
  }
}

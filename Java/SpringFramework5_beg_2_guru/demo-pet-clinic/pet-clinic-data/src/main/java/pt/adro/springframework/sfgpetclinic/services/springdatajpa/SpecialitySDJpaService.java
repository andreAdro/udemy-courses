package pt.adro.springframework.sfgpetclinic.services.springdatajpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pt.adro.springframework.sfgpetclinic.model.Speciality;
import pt.adro.springframework.sfgpetclinic.repositories.SpecialityRepository;
import pt.adro.springframework.sfgpetclinic.services.SpecialityService;

/**
 * @author Adro @ 2020-01-08
 */
@Service
@Profile("springdatajpa")
public class SpecialitySDJpaService extends AbstractCRUDService<Speciality, Long> implements
    SpecialityService {

  private SpecialityRepository specialityRepository;

  @Autowired
  public SpecialitySDJpaService(SpecialityRepository specialityRepository) {
    super(specialityRepository);
    this.specialityRepository = specialityRepository;
  }
}

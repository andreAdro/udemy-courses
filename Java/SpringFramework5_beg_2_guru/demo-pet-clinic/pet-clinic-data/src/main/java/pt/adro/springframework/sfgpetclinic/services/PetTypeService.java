package pt.adro.springframework.sfgpetclinic.services;

import pt.adro.springframework.sfgpetclinic.model.PetType;

/** Created by: adroa Date: 2019-10-03 */
public interface PetTypeService extends CrudService<PetType, Long> {}

package pt.adro.springframework.sfgpetclinic.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "OWNERS")
public class Owner extends Person {

  private static final long serialVersionUID = -8160049958008457232L;

  @Column(name = "ADDRESS")
  private String address;

  @Column(name = "CITY")
  private String city;

  @Column(name = "TELEPHONE")
  private String telephone;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "owner")
  private Set<Pet> pets = new HashSet<>();

  @Builder
  public Owner(Long id, String firstName, String lastName, String address, String city,
      String telephone, Set<Pet> pets) {
    super(id, firstName, lastName);
    this.address = address;
    this.city = city;
    this.telephone = telephone;

    if (pets != null) {
      this.pets = pets;
    }
  }

  public Pet getPet(String name, boolean ignoreNew) {
    name = name.toLowerCase();
    for (Pet pet : pets) {
      if (!ignoreNew || !pet.isNew()) {
        String compareName = pet.getName();
        compareName = compareName.toLowerCase();
        if (compareName.equalsIgnoreCase(name)) {
          return pet;
        }
      }
    }
    return null;
  }
}

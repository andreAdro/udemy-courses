package pt.adro.springframework.sfgpetclinic.services;

import pt.adro.springframework.sfgpetclinic.model.Visit;

/**
 * @author Adro @ 2020-01-12
 */
public interface VisitService extends CrudService<Visit, Long> {

}

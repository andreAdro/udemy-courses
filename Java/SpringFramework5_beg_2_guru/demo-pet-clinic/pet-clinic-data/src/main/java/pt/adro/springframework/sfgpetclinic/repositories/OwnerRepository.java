package pt.adro.springframework.sfgpetclinic.repositories;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pt.adro.springframework.sfgpetclinic.model.Owner;

/**
 * @author Adro @ 2020-01-08
 */
public interface OwnerRepository extends JpaRepository<Owner, Long> {

  Owner findByLastName(String lastName);

  @Query("from OWNERS o where o.lastName like %:lastName%")
  List<Owner> findAllByLastNameLike(String lastName);
}

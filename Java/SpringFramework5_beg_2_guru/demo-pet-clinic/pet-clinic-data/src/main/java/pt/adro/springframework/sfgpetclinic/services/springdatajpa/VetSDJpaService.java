package pt.adro.springframework.sfgpetclinic.services.springdatajpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pt.adro.springframework.sfgpetclinic.model.Vet;
import pt.adro.springframework.sfgpetclinic.repositories.VetRepository;
import pt.adro.springframework.sfgpetclinic.services.VetService;

/**
 * @author Adro @ 2020-01-08
 */
@Service
@Profile("springdatajpa")
public class VetSDJpaService extends AbstractCRUDService<Vet, Long> implements VetService {

  private VetRepository vetRepository;

  @Autowired
  public VetSDJpaService(VetRepository vetRepository) {
    super(vetRepository);
    this.vetRepository = vetRepository;
  }
}

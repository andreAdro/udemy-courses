package pt.adro.springframework.sfgpetclinic.services.springdatajpa;

import java.util.HashSet;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import pt.adro.springframework.sfgpetclinic.model.BaseEntity;
import pt.adro.springframework.sfgpetclinic.services.CrudService;

/**
 * @author Adro @ 2020-01-08
 */
abstract class AbstractCRUDService<T extends BaseEntity, ID extends Long>
    implements CrudService<T, ID> {

  private JpaRepository<T, ID> jpaRepository;

  public AbstractCRUDService(JpaRepository<T, ID> jpaRepository) {
    this.jpaRepository = jpaRepository;
  }

  @Override
  public Set<T> findAll() {
    return new HashSet<>(this.jpaRepository.findAll());
  }

  @Override
  public T findById(ID id) {
    return this.jpaRepository.findById(id).orElse(null);
  }

  @Override
  public T save(T object) {
    return this.jpaRepository.save(object);
  }

  @Override
  public void delete(T object) {
    this.jpaRepository.save(object);
  }

  @Override
  public void deleteById(ID id) {
    this.jpaRepository.deleteById(id);
  }

}

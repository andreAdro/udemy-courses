package pt.adro.springframework.sfgpetclinic.services.map;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import pt.adro.springframework.sfgpetclinic.model.Owner;
import pt.adro.springframework.sfgpetclinic.model.Pet;
import pt.adro.springframework.sfgpetclinic.services.CrudService;
import pt.adro.springframework.sfgpetclinic.services.OwnerService;
import pt.adro.springframework.sfgpetclinic.services.PetService;
import pt.adro.springframework.sfgpetclinic.services.PetTypeService;

/**
 * Created by: adroa Date: 2019-09-14
 */
@Service
@Profile({"default", "map"})
public class OwnerServiceMap extends AbstractMapService<Owner, Long>
    implements CrudService<Owner, Long>, OwnerService {

  private final PetTypeService petTypeService;
  private final PetService petService;

  @Autowired
  public OwnerServiceMap(PetTypeService petTypeService, PetService petService) {
    this.petTypeService = petTypeService;
    this.petService = petService;
  }

  @Override
  public Owner save(Owner object) {
    if (object != null) {
      if (object.getPets() != null) {
        object
            .getPets()
            .forEach(
                pet -> {
                  if (pet.getPetType() != null) {
                    if (pet.getPetType().getId() == null) {
                      pet.setPetType(petTypeService.save(pet.getPetType()));
                    }
                  } else {
                    throw new RuntimeException("Pet Type is required!");
                  }
                  if (pet.getId() == null) {
                    Pet savedPet = petService.save(pet);
                    pet.setId(savedPet.getId());
                  }
                });
      }
      return super.save(object);
    }
    return null;
  }

  @Override
  public Owner findByLastName(String lastName) {
    return this.findAll()
        .stream()
        .filter(owner -> owner.getLastName().equalsIgnoreCase(lastName))
        .findFirst()
        .orElse(null);
  }

  @Override
  public List<Owner> findAllByLastNameLike(String lastName) {
    return this.findAll()
        .stream()
        .filter(owner -> owner.getLastName().equalsIgnoreCase(lastName))
        .collect(Collectors.toList());
  }
}

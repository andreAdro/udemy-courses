package pt.adro.springframework.sfgpetclinic.services.springdatajpa;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import pt.adro.springframework.sfgpetclinic.model.Owner;
import pt.adro.springframework.sfgpetclinic.repositories.OwnerRepository;
import pt.adro.springframework.sfgpetclinic.services.OwnerService;

/**
 * @author Adro @ 2020-01-08
 */
@Service
@Profile("springdatajpa")
public class OwnerSDJpaService extends AbstractCRUDService<Owner, Long> implements OwnerService {

  private OwnerRepository ownerRepository;

  @Autowired
  public OwnerSDJpaService(OwnerRepository ownerRepository) {
    super(ownerRepository);
    this.ownerRepository = ownerRepository;
  }

  @Override
  public Owner findByLastName(String lastName) {
    return ownerRepository.findByLastName(lastName);
  }

  @Override
  public List<Owner> findAllByLastNameLike(String lastName) {
    return ownerRepository.findAllByLastNameLike(lastName);
  }

}

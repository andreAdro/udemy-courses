package pt.adro.springframework.sfgpetclinic.services;

import pt.adro.springframework.sfgpetclinic.model.Vet;

public interface VetService extends CrudService<Vet, Long> {}

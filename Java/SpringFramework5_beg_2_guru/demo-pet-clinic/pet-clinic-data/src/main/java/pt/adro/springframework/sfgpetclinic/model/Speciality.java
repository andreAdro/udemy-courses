package pt.adro.springframework.sfgpetclinic.model;

import javax.persistence.Entity;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by: adroa Date: 2019-10-03
 */
@Getter
@Setter
@Entity(name = "SPECIALITIES")
public class Speciality extends BaseEntity {

  private static final long serialVersionUID = 85771945675994621L;

  private String description;
}

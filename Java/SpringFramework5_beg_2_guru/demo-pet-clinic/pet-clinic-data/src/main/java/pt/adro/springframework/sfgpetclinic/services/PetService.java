package pt.adro.springframework.sfgpetclinic.services;

import pt.adro.springframework.sfgpetclinic.model.Pet;

public interface PetService extends CrudService<Pet, Long> {

}

package pt.adro.springframework.sfgpetclinic.services;

import pt.adro.springframework.sfgpetclinic.model.Speciality;

/** Created by: adroa Date: 2019-10-03 */
public interface SpecialityService extends CrudService<Speciality, Long> {}

package pt.adro.springframework.sfgpetclinic.services.map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pt.adro.springframework.sfgpetclinic.model.Speciality;
import pt.adro.springframework.sfgpetclinic.model.Vet;
import pt.adro.springframework.sfgpetclinic.services.CrudService;
import pt.adro.springframework.sfgpetclinic.services.SpecialityService;
import pt.adro.springframework.sfgpetclinic.services.VetService;

/** Created by: adroa Date: 2019-09-14 */
@Service
@Profile({"default", "map"})
public class VetServiceMap extends AbstractMapService<Vet, Long>
    implements CrudService<Vet, Long>, VetService {

  private SpecialityService specialityService;

  @Autowired
  public VetServiceMap(SpecialityService specialityService) {
    this.specialityService = specialityService;
  }

  @Override
  public Vet save(Vet object) {
    if (object.getSpecialities().size() > 0) {
      object
          .getSpecialities()
          .forEach(
              speciality -> {
                if (speciality.getId() == null) {
                  Speciality savedSpeciality = specialityService.save(speciality);
                  speciality.setId(savedSpeciality.getId());
                }
              });
    }
    return super.save(object);
  }
}

package pt.adro.springframework.sfgpetclinic.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "VETS")
public class Vet extends Person {

  private static final long serialVersionUID = -4636867186773404684L;

  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(name = "VETS_SPECIALITIES",
      joinColumns = @JoinColumn(name = "VET_ID"),
      inverseJoinColumns = @JoinColumn(name = "SPECIALITY_ID"))
  private Set<Speciality> specialities = new HashSet<>();
}

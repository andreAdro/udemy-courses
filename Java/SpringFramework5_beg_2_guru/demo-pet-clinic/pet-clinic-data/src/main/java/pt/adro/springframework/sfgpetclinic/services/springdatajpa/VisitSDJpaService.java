package pt.adro.springframework.sfgpetclinic.services.springdatajpa;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pt.adro.springframework.sfgpetclinic.model.Visit;
import pt.adro.springframework.sfgpetclinic.repositories.VisitRepository;
import pt.adro.springframework.sfgpetclinic.services.VisitService;

/**
 * @author Adro @ 2020-01-12
 */
@Service
@Profile("springdatajpa")
public class VisitSDJpaService extends AbstractCRUDService<Visit, Long> implements VisitService {

  private VisitRepository visitRepository;

  public VisitSDJpaService(VisitRepository visitRepository) {
    super(visitRepository);
    this.visitRepository = visitRepository;
  }
}

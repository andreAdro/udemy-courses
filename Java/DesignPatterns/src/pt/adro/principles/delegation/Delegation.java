package pt.adro.principles.delegation;

// The delegate
class RealPrinter {

  void print() {
    System.out.println("The Delegate");
  }
}

// The delegator
class Printer {

  RealPrinter realPrinter = new RealPrinter();

  void print() {
    realPrinter.print();
  }
}

public class Delegation {

  public static void main(String[] args) {
    Printer printer = new Printer();
    printer.print();
  }

}

package pt.adro.principles.interface_segregation;

public interface Restaurant {

  void acceptOnlineOrder();

  void takeTelephoneOrder();

  void payOnline();

  void walkInCustomerOrder();

  void payInPerson();

}


class OnlineClientImpl implements Restaurant {

  @Override
  public void acceptOnlineOrder() {

  }

  @Override
  public void takeTelephoneOrder() {

  }

  @Override
  public void payOnline() {

  }

  @Override
  public void walkInCustomerOrder() {

  }

  @Override
  public void payInPerson() {

  }
}
package pt.adro.principles.interface_segregation;

interface Shape {

  double area();

}

interface SolidShape {

  double volume();

}

interface ManageShape {

  double calculate();
}

class Cube implements Shape, SolidShape, ManageShape {

  @Override
  public double calculate() {
    return this.area() * this.volume();
  }

  @Override
  public double area() {
    return 0;
  }

  @Override
  public double volume() {
    return 0;
  }
}

class Square implements Shape, ManageShape {

  @Override
  public double area() {
    return 0;
  }

  @Override
  public double calculate() {
    return this.area();
  }
}

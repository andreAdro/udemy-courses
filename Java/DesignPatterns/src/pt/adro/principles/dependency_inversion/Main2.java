package pt.adro.principles.dependency_inversion;


class Client {

  private ExampleService service;

  public Client() {
    service = new ExampleService();
  }

  public String greet() {
    return "Hello from greet!";
  }
}

class ExampleService {

}

public class Main2 {

}

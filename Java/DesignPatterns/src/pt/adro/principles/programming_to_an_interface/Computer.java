package pt.adro.principles.programming_to_an_interface;


interface DisplayModule {

  void display();
}

class Monitor implements DisplayModule {

  @Override
  public void display() {
    System.out.println("Display through Monitor.");
  }
}

class Projector implements DisplayModule {

  @Override
  public void display() {
    System.out.println("Display through projector.");
  }
}

public class Computer {

  DisplayModule displayModule;

  public void setDisplayModule(DisplayModule displayModule) {
    this.displayModule = displayModule;
  }

  public void display() {
    displayModule.display();
  }

  public static void main(String[] args) {
    Computer computer = new Computer();
    DisplayModule monitor = new Monitor();
    DisplayModule projector = new Projector();

    computer.setDisplayModule(monitor);
    computer.display();
    computer.setDisplayModule(projector);
    computer.display();
  }
}

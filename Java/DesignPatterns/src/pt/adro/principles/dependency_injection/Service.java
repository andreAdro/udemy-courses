package pt.adro.principles.dependency_injection;

public interface Service {

  void write(String message);
}

class ServiceA implements Service {

  @Override
  public void write(String message) {
    System.out.println(message);
  }
}

class ConstructorClient {

  private Service service;

  public ConstructorClient(Service service) {
    this.service = service;
  }

  void doSomething() {
    this.service.write("");
  }
}

class SetterClient {

  private Service service;

  public void setService(Service service) {
    this.service = service;
  }

  void doSomething() {
    this.service.write("");
  }
}

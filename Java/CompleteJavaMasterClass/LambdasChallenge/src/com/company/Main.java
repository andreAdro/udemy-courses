package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
//        firstChallenge();
//        secondThirdChallenge();
//        fourthFifthChallenge();
//        sixthSevenChallenge();
//        ninthChallenge();
//        tenthChallenge();
//        eleventhChallenge();
//        twelveChallenge();
        fourteenChallenge();
    }


    private static void firstChallenge() {
        // write the following anonymous class as a lambda expression
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                String myString = "Let's split this up into an array";
                String[] parts = myString.split(" ");
                for (String part : parts) {
                    System.out.println(part);
                }
            }
        };
        runnable.run();

        System.out.println("\nImplementation:");
        Runnable runnable2 = () -> {
            String myString = "Let's split this up into an array";
            String[] parts = myString.split(" ");
            List<String> partsList = Arrays.asList(parts);
            partsList.forEach(s -> System.out.println(s));
        };
        runnable2.run();
    }

    private static void secondThirdChallenge() {
        String source = "1234567890";
        System.out.println(everySecondChar(source));

//        Function<String, String> lamdaFunction = s -> {
//            StringBuilder returnVal = new StringBuilder();
//            for (int i = 0; i < source.length(); i++) {
//                if (i % 2 == 1) {
//                    returnVal.append(source.charAt(i));
//                }
//            }
//            return returnVal.toString();
//        };
//        System.out.println(lamdaFunction.apply(source));
    }

    private static String everySecondChar(String source) {
        StringBuilder returnVal = new StringBuilder();
        for (int i = 0; i < source.length(); i++) {
            if (i % 2 == 1) {
                returnVal.append(source.charAt(i));
            }
        }
        return returnVal.toString();
    }

    private static void fourthFifthChallenge() {
        String source = "1234567890";
        Function<String, String> lamdaFunction = s -> {
            StringBuilder returnVal = new StringBuilder();
            for (int i = 0; i < source.length(); i++) {
                if (i % 2 == 1) {
                    returnVal.append(source.charAt(i));
                }
            }
            return returnVal.toString();
        };
        System.out.println(everySecondCharLambda(lamdaFunction, source));
    }

    private static String everySecondCharLambda(Function<String, String> function, String parameter) {
        return function.apply(parameter);
    }

    private static void sixthSevenChallenge() {
        Supplier<String> iLoveJava = () -> "I love Java!";
        String supplierResult = iLoveJava.get();
        System.out.println(supplierResult);
    }

    private static void ninthChallenge() {
        List<String> topNames25 = Arrays.asList("Amelia", "Olivia", "emily", "Isla", "Ava", "oliver",
                "Jack", "Charlie", "harry", "Jacob");

        List<String> upperCasedTopNames = topNames25.stream().map(s -> {
            String upperCased = s.substring(0, 1).toUpperCase();
            return upperCased.concat(s.substring(1, s.length()));
        }).sorted().collect(Collectors.toList());
        upperCasedTopNames.forEach(s -> System.out.println(s));
    }

    private static void tenthChallenge() {
        List<String> topNames25 = Arrays.asList("Amelia", "Olivia", "emily", "Isla", "Ava", "oliver",
                "Jack", "Charlie", "harry", "Jacob");

        List<String> firstUpperCasedList = new ArrayList<>();
        topNames25.forEach(name -> firstUpperCasedList.add(name.substring(0, 1).toUpperCase() + name.substring(1)));
        firstUpperCasedList.sort(String::compareTo);
        firstUpperCasedList.forEach(System.out::println);
    }

    private static void eleventhChallenge() {
        List<String> topNames25 = Arrays.asList("Amelia", "Olivia", "emily", "Isla", "Ava", "oliver",
                "Jack", "Charlie", "harry", "Jacob");

        topNames25.stream()
                .map(s -> s.substring(0, 1).toUpperCase() + s.substring(1))
                .sorted()
                .forEach(System.out::println);
    }

    private static void twelveChallenge() {
        List<String> topNames25 = Arrays.asList("Amelia", "Olivia", "emily", "Isla", "Ava", "oliver",
                "Jack", "Charlie", "harry", "Jacob");

        long namesWithA = topNames25.stream()
                .filter(s -> s.toUpperCase().contains("A"))
                .count();
        System.out.println(namesWithA);
    }

    private static void fourteenChallenge() {
        List<String> topNames25 = Arrays.asList("Amelia", "Olivia", "emily", "Isla", "Ava", "oliver",
                "Jack", "Charlie", "harry", "Jacob");

        topNames25
                .stream()
                .map(name -> name.substring(0, 1).toUpperCase() + name.substring(1))
                .peek(System.out::println)
                .sorted(String::compareTo)
                .count();
    }

}
